---
title:  Resume
comments: false
---

<object data="static/Shelby_Hayes_Resume_01092021.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="static/Shelby_Hayes_Resume_01092021.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="static/Shelby_Hayes_Resume_01092021.pdf">Download PDF</a>.</p>
    </embed>
</object>

<embed src="https://drive.google.com/viewerng/
viewer?embedded=true&url=https://gitlab.com/shelbyhayes/data-visualization/-/blob/master/static/Shelby_Hayes_Resume_01092021.pdf" width="500" height="375">

![Resume](./static/Shelby_Hayes_Resume_01092021.pdf "Shelby Hayes Resume")

<iframe src="themes/beautifulhugo/static/img/Shelby_Hayes_Resume.PNG" width="100%" height="100%"></iframe>

<img= themes/beautifulhugo/static/img/Shelby_Hayes_Resume.PNG/>

### If you have any questions email me at:
shelby.hayes@microsoft.com
