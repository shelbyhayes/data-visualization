---
title: About me
subtitle: Miniture Bio
comments: false
---
![Photo](https://gitlab.com/shelbyhayes/data-visualization/blob/master/content/page/_DSC3210.jpg)

My name is Shelby Hayes. I am passionate about Data, Security, and People. I was an undergraduate research assistant
in college focusing on analysis of biological data and modeling physical systems. I am currently learning about
digital forensics, and OSINT.

In my free time I enjoy:

- Hiking, Paddle Boarding, and Reading
- Drinking Coffee 
- Helping people
- Learning new things

### If you have any questions email me at:
shelby.hayes@microsoft.com
