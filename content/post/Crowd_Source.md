---
title: "CrowdSourceData"
subtitle: "Basic data exploration and explanation with a Crowd Sourced Dataset"
author: "Shelby Hayes"
date: '2018-11-13'
post-entry: hide

output:
html_document:
keep_md: yes
knit: (function(inputFile, encoding) {
  ofi <- paste0(gsub(".Rmd","",inputFile),"_.md");
  rmarkdown::render(inputFile, encoding=encoding, output_file=ofi) })
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

##Data Preprocessing
```{r, warning=FALSE, message=FALSE}
  require(jsonlite)
  require(tidyr)
  require(dplyr)
  require(magrittr)
  
  options(stringsAsFactors=TRUE)
  crowdSourceRaw <- jsonlite::fromJSON("data.json")
  crowdSourceRaw<-crowdSourceRaw[['data']]
  
  crowdSourceJSON <- jsonlite::toJSON(crowdSourceRaw)
  
  crowdSource <-jsonlite::fromJSON(crowdSourceJSON,flatten=TRUE)
  
  crowdSource$category<-factor(crowdSource$category)
  crowdSource$event_name<-factor(crowdSource$event_name)
  crowdSource$gender<-factor(crowdSource$gender)
  crowdSource$age<-factor(crowdSource$age)
  crowdSource$marital_status<-factor(crowdSource$marital_status)
  
  crowdSource<-dplyr::select(crowdSource,-session_id)
  crowdSource<-dplyr::select(crowdSource,-client_time)
  
  crowdSource$device<-factor(crowdSource$device)
  crowdSource$location.state<-factor(crowdSource$location.state)
  
  ## remove NA's in amount
  crowdSource<-crowdSource %>% tidyr::replace_na(list(amount=0))
```





##Bar Chart 1 Variable
```{r}
#str(crowdSource)
library(ggplot2)
levels(crowdSource$gender)[1] <-"Female"
levels(crowdSource$gender)[2] <-"Male"
levels(crowdSource$gender)[3] <-"Unknown"
forBarPlot<-ggplot(crowdSource, aes(gender))
forBarPlot +
  geom_bar(width = 0.5, fill="purple") +
  labs(title="Number of Each Gender in Study", x="Gender", y="Count")
```
<img style="-webkit-user-select: none" src="http://127.0.0.1:45906/chunk_output/s/6122B4FD/chpl614m8um1t/000003.png">

 Our crowdsource dataset seems to be balanced, between Male and Females. We do have a few unknowns, and we should take them into consideration when modeling our data. 


##Bar Chart 1 Variable
```{r}
#str(crowdSource)
library(ggplot2)
forBarPlot2<-ggplot(crowdSource, aes(x=gender, fill=marital_status))
forBarPlot2 +
  geom_bar(width = 0.5, position = position_dodge()) +
  labs(title="Gender Marital Status", x="Gender", fill="Marital Status", y="Count")
```
<img style="-webkit-user-select: none" src="http://127.0.0.1:45906/chunk_output/s/6122B4FD/ctb73yeqwkh4v/000003.png">

From the last graph, we know the distribution of the gender data, we are adding the marital status in this graph. We see that most individuals are married, but the proportion of the population of married to single seems to be equivalent between genders. Again, we must note the Unknown gender category.

##Pie Chart
```{r}
pie <- ggplot(crowdSource, aes(x = "", fill = age)) + 
  geom_bar(width = 1) +
  theme(axis.line = element_blank(), 
        plot.title = element_text(hjust=0.5)) + 
  labs(fill="Age Range", 
       x=NULL, 
       y=NULL, 
       title="Age Ranges in CrowdSource")
  
pie + coord_polar(theta = "y", start=0)
```
<img style="-webkit-user-select: none" src="http://127.0.0.1:45906/chunk_output/s/6122B4FD/chpl614m8um1t/000003.png">


 In this pie chart, we observe the amount of 18-24 year olds is roughly half of our dataset's composition. This means, we have anomalous data in our age variable. We recognize this could pose an issue when comparing age ranges, we should keep this in mind when creating future models.

##Waffle Chart
```{r}
library(waffle)
var <- crowdSource$device

## Prep data (nothing to change here)
nrows <- 10
df <- expand.grid(y = 1:nrows, x = 1:nrows)
categ_table <- round(table(var) * ((nrows*nrows)/(length(var))))
categ_table
waffle(categ_table, rows=10, title="Waffle Chart of Device Type")
```


\n In this mosaic plot we see there are more iOS users than android users. So, we have more anomalous data in our dataset, again, that we should keep in mind. 

##2 factor Mosaic
```{r}
realMosaic1<-table(crowdSource$gender,crowdSource$age)
mosaicplot(realMosaic1, color = T, main="Age Range and Gender")
```


\n From this Mosaic plot, we can confirm our previous thoughts regarding equal split in genders. We can also see that ages 18-24 make up a larger percent of our dataset's population. We should keep this anomaly in mind when modeling the data.

##Mosaic 2
```{r}
library(vcd)
library(RColorBrewer)
mycol<-brewer.pal(10,"Blues")
table<-structable(~gender+age+marital_status, data=crowdSource)
mosaicplot(table, color = mycol, main = "Demographic in Sample", xlab = "Gender", ylab = "Age Range")
```


\n This mosaic plot is the most interesting we've seen thus far. We can see that of Females, age 18-24, majority are married. We can also note that the graph looks squished, thus the split between genders is misleading and not different from the other graphs. We can confirm that there are fewer 35-55+ ages, because their rows are smaller than the 18-24 and 25-34 age ranges. 



##Mosaic 3
```{r, warning=FALSE}
library(RColorBrewer)
library(vcd)
levels(crowdSource$gender)[1] <-"F"
levels(crowdSource$gender)[2] <-"M"
levels(crowdSource$gender)[3] <-"U"
table<-structable(~category+age+marital_status+gender, data=crowdSource)
mosaicplot(table, color = display.brewer.all(type="seq"), main = "Sample Demographics and Magazine Category", xlab = "Category", ylab = "Age Range")
```


\n This mosaic plot is really helpful for us becuase there are so many categories involved. we can see that of those who like Technology, are 18-24, and female are likely to be married. We can take a "deep dive" into this data and explore more about our sample. We can see that of those aged 35-44 who like Fashion, are more evenly split between married and single. The same logic follows with those who like Games. We can go through this mosaic and see which groups are likely what demographic. 
